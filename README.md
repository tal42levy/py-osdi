# py-osdi

py-osdi offers a python framework for integrating with Open Supporter Data Interface-compliant
systems, most notably ActionNetwork and ActionBuilder

It is currently in early alpha as it is being developed primarily based  on the needs of
the Democratic Socialists of America, rather than for complete API integration

## Currently implemented

* Modeling, querying for, updating, and creating new actionnetwork and actionbuilder people
* handling tags, taggings, and custom fields

## Example
```
from osdi.action_network.service import ActionNetworkService
email = "berniesanders@dsausa.org"

an_service = ActionNetworkService("https://actionnetwork.org/api/v2/", "INSERT API KEY HERE")
per = an_service.get_person({"email_address": email})
an_person.update_field("Join Date", "2020-01-01")
an_person.update_tag("Member In Good Standing", True)
an_person.update() # Must call update to commit changes

ab_service = ActionBuilderService("https://myabsetup.actionbuilder.org/api/rest/v1", "INSERT API KEY HERE")
campaign = ab_service.campaigns["Central Branch"]
ab_member = an_service.get_person({"email_address": email})
if ab_member is None:
    ab_member = ab_camp.create_person("Bernie", "Sanders", email)
ab_person.update_tagging("Membership Details", "Key Dates", "Join Date", "20201-01-01")
ab_person.update_tagging("Events Attended", "Canvasses", "Super Tuesday GOTV", True)
ab_person.update()
```
